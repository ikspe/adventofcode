#!/bin/bash

cut -d\  -f1,3 input | while read line; do
    FROM=$(echo "$line" | cut -d\  -f1)
    TO=$(echo "$line" | cut -d\  -f2)
    sed -e "s/${FROM}/${TO}
done
