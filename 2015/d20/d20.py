from math import sqrt
def gifts(n):
    r = 0
    for i in range(1, 1 + int(sqrt(n))):
        if n % i == 0:
            r = r + 10 * i + 10 * (n / i)
            if i * i == n:
                r = r - 10 * i
    return r
i = 0
while True:
    i = i + 1
    if gifts(i) > 29000000:
        break
print(i)
