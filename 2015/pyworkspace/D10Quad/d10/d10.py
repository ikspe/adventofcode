import re

p3 = re.compile(r'(.)\1\1')
p2 = re.compile(r'(.)\1')

inputNb = "1321131112"

def iterate(inputStr, remaining):
    finished = False
    i = 0
    result = ""   
    while(not finished):
        curNb = inputStr[i:i + 1]
        curNb2 = ""
        curNb3 = ""
        if (i < len(inputStr) - 1):
            curNb2 = inputStr[i:i + 2]
        if (i < len(inputStr) - 2):
            curNb3 = inputStr[i:i + 3]

        processFurther = True
        if(curNb3 != ""):
            if(p3.match(curNb3)):
                processFurther = False
                result = result + "3" + curNb
                i = i + 3
                finished = (i >= len(inputStr))

        if (processFurther):
            if(curNb2 != ""):
                if(p2.match(curNb2)):
                    processFurther = False
                    result = result + "2" + curNb
                    i = i + 2
                    finished = (i >= len(inputStr))

            if (processFurther):
                result = result + "1" + curNb
                i = i + 1
                finished = (i >= len(inputStr))

    if(remaining == 0):
        return result
    
    print(str(remaining) + ": " + str(len(result)))
    return iterate(result, remaining - 1)

if __name__ == '__main__':
    print(iterate(inputNb, 49))
