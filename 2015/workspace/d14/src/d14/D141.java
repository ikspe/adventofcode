package d14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D141 {

	// Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.
	private static final Pattern P = Pattern.compile("^([^ ]+)[^0-9]*(\\d+)[^0-9]*(\\d+)[^0-9]*(\\d+).*");

	private static final Integer RACE_DURATION = 2503;

	private static List<Reindeer> racers = new ArrayList<Reindeer>();

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					String name = m.group(1);
					Integer speed = Integer.valueOf(m.group(2));
					Integer sprintDuration = Integer.valueOf(m.group(3));
					Integer restDuration = Integer.valueOf(m.group(4));
					racers.add(new Reindeer(name, speed, sprintDuration, restDuration));
				} else {
					System.err.println("ERROR: " + line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		List<Integer> distances = new ArrayList<Integer>();

		for (Reindeer r : racers) {
			distances.add(distanceAfterSec(r, RACE_DURATION));
		}
		System.out.println(max(distances));
	}

	private static Integer distanceAfterSec(Reindeer r, Integer seconds) {
		Integer cycleDuration = r.getRestDuration() + r.getSprintDuration();
		Integer nbCycles = seconds / cycleDuration;
		Integer distance = nbCycles * r.getSpeed() * r.getSprintDuration();

		Integer remainingTime = seconds - nbCycles * cycleDuration;
		distance += Math.min(remainingTime, r.getSprintDuration()) * r.getSpeed();
		return distance;
	}

	private static Integer max(List<Integer> distances) {
		Integer max = distances.get(0);
		for (Integer d : distances) {
			if (d > max)
				max = d;
		}
		return max;
	}
}
