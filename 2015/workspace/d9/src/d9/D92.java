package d9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D92 {

	private static final Pattern P = Pattern.compile("([a-zA-Z]*) to ([a-zA-Z]*) = ([0-9]*)");

	private static List<Trajet> trajets = new LinkedList<Trajet>();

	public static void main(String[] args) throws IOException {
		buildTrajets();
		List<String> cities = Arrays.asList(new String[] { "AlphaCentauri", "Tristram", "Snowdin", "Tambi", "Faerun",
				"Norrath", "Straylight", "Arbre" });
		List<Integer> distances = new LinkedList<Integer>();
		for (String start : cities) {
			List<String> next = new LinkedList<String>();
			next.addAll(cities);
			next.remove(start);
			Etape e = new Etape(start, 0, next);
			distances.add(maxDistance(e));
		}
		System.out.println(max(distances));
	}

	private static void buildTrajets() throws IOException {
		File f = new File("input");
		BufferedReader b = new BufferedReader(new FileReader(f));
		String line = null;
		while ((line = b.readLine()) != null) {
			Matcher m = P.matcher(line);
			if (m.matches()) {
				int len = Integer.parseInt(m.group(3));
				String from = m.group(1);
				String to = m.group(2);
				trajets.add(new Trajet(from, to, len));
				trajets.add(new Trajet(to, from, len));
			}
		}
		b.close();
	}

	private static Integer maxDistance(Etape e) {
		if (e.getNext().size() == 0) {
			return e.getDistance();
		} else {
			List<Integer> distances = new LinkedList<Integer>();
			for (String v : e.getNext()) {
				List<String> next = new LinkedList<String>();
				next.addAll(e.getNext());
				next.remove(v);
				Etape e2 = new Etape(v, e.getDistance() + getDistance(e.getVille(), v), next);
				distances.add(maxDistance(e2));
			}
			return max(distances);
		}
	}

	private static Integer getDistance(String ville, String v) {
		for (Trajet t : trajets) {
			if (ville.equals(t.getFrom()) && v.equals(t.getTo())) {
				return t.getLength();
			}
		}
		return null;
	}

	private static Integer max(List<Integer> distances) {
		Integer max = distances.get(0);
		for (Integer d : distances) {
			if (d > max)
				max = d;
		}
		return max;
	}
}
