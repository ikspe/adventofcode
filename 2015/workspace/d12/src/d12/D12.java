// does not work
package d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class D12 {

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			String input = "";
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				input += line;
			}
			Object json = new JSONParser().parse(input);
			Class<? extends Object> c = json.getClass();

			if (c.isAssignableFrom(JSONAware.class)) {
				JSONAware jsonItem = (JSONAware) json;
				jsonItem = removeRed(jsonItem);
				System.out.println(jsonItem.toJSONString());
			}
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static JSONAware removeRed(JSONAware json) {
		if (JSONArray.class.equals(json)) {
			JSONArray array = (JSONArray) json;
			Iterator<JSONAware> it = (Iterator<JSONAware>) array.iterator();
			while (it.hasNext()) {
				Object o = it.next();
				if (o.getClass().isAssignableFrom(JSONAware.class)) {
					array.remove(o);
					JSONAware child = (JSONAware) o;
					if (!containsRed(child)) {
						array.add(removeRed(child));
					}
				}
			}
			return array;
		} else if (JSONObject.class.equals(json)) {
			JSONObject obj = (JSONObject) json;
			for (Object o : obj.keySet()) {
				Object v = obj.get(o);
				if (o.getClass().isAssignableFrom(JSONAware.class)) {
					obj.remove(o);
					JSONAware child = (JSONAware) o;
					if (!containsRed(child)) {
						obj.put(removeRed(child), v);
					}
				}

				if (v.getClass().isAssignableFrom(JSONAware.class)) {
					obj.remove(o);
					JSONAware child = (JSONAware) v;
					if (!containsRed(child)) {
						obj.put(o, removeRed(child));
					}
				}
			}
			return obj;
		} else {
			System.err.println("Wrong class: " + json.getClass() + " (string: ' " + json.toJSONString() + " ')");
			return null;
		}
	}

	private static boolean containsRed(JSONAware json) {
		if (JSONArray.class.equals(json)) {
			JSONArray array = (JSONArray) json;
			Iterator<JSONAware> it = (Iterator<JSONAware>) array.iterator();
			while (it.hasNext()) {
				Object o = it.next();
				if (String.class.equals(o.getClass())) {
					String s = (String) o;
					if (s.contains("red")) {
						return true;
					}
				}
			}
		} else if (JSONObject.class.equals(json)) {
			JSONObject obj = (JSONObject) json;
			for (Object o : obj.keySet()) {
				if (String.class.equals(o.getClass())) {
					String s = (String) o;
					if (s.contains("red")) {
						return true;
					}
				}

				Object v = obj.get(o);
				if (String.class.equals(v.getClass())) {
					String s = (String) v;
					if (s.contains("red")) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
