package d15;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D15 {

	// Butterscotch: capacity -1, durability 0, flavor 5, texture 0, calories 6
	private static final Pattern P = Pattern
			.compile("^([^:]+)[^-0-9]*([-0-9]+)[^-0-9]*([-0-9]+)[^-0-9]*([-0-9]+)[^-0-9]*([-0-9]+)[^-0-9]*([-0-9]+).*");

	private static List<Ingredient> kitchen = new ArrayList<Ingredient>();

	private static final Integer MAX_SPOONS = 100;

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					String name = m.group(1);
					Integer capacity = Integer.valueOf(m.group(2));
					Integer durability = Integer.valueOf(m.group(3));
					Integer flavor = Integer.valueOf(m.group(4));
					Integer texture = Integer.valueOf(m.group(5));
					Integer calories = Integer.valueOf(m.group(6));
					kitchen.add(new Ingredient(name, capacity, durability, flavor, texture, calories));
				} else {
					System.err.println("ERROR: " + line);
				}
			}
			Map<Map<Ingredient, Integer>, Integer> scores = new HashMap<Map<Ingredient, Integer>, Integer>();

			Ingredient i1 = kitchen.get(0);
			Ingredient i2 = kitchen.get(1);
			Ingredient i3 = kitchen.get(2);
			Ingredient i4 = kitchen.get(3);

			for (int i = 0; i <= MAX_SPOONS; i++) {
				System.out.println(i + "/" + MAX_SPOONS);
				for (int j = 0; j <= MAX_SPOONS; j++) {
					for (int k = 0; k <= MAX_SPOONS; k++) {
						int l = MAX_SPOONS - i - j - k;
						if (l >= 0) {
							Map<Ingredient, Integer> receipe = new HashMap<Ingredient, Integer>();
							receipe.put(i1, i);
							receipe.put(i2, j);
							receipe.put(i3, k);
							receipe.put(i4, l);
							Integer score = receipeScore(receipe);
							if (score > 0)
								scores.put(receipe, score);
						}
					}
				}

				// XXX
				/*
				 * int l = MAX_SPOONS - i; if (l >= 0) { Map<Ingredient,
				 * Integer> receipe = new HashMap<Ingredient, Integer>();
				 * receipe.put(i1, i); receipe.put(i2, l); Integer score =
				 * receipeScore(receipe); if (score > 0) scores.put(receipe,
				 * score); }
				 */
				// XXX

			}
			// displayScores(scores);
			maxScore(scores);
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void displayScores(Map<Map<Ingredient, Integer>, Integer> scores) {
		for (Map<Ingredient, Integer> receipe : scores.keySet()) {
			String result = "";
			Integer score = scores.get(receipe);
			for (Ingredient igdt : receipe.keySet()) {
				if (result.equals(""))
					result = receipe.get(igdt) + " x " + igdt;
				else
					result += " + " + receipe.get(igdt) + " x " + igdt;
			}
			result += " = " + score;
			System.out.println(result);
		}
	}

	private static void maxScore(Map<Map<Ingredient, Integer>, Integer> scores) {
		System.out.println("");
		Integer result = 0;
		Map<Ingredient, Integer> bestReceipe = null;
		for (Map<Ingredient, Integer> receipe : scores.keySet()) {
			Integer score = scores.get(receipe);
			if (score > result) {
				bestReceipe = receipe;
				result = score;
			}
		}
		System.out.println(bestReceipe + " -> " + result);
	}

	private static Integer receipeScore(Map<Ingredient, Integer> receipe) {
		Integer capacity = 0;
		Integer durability = 0;
		Integer flavor = 0;
		Integer texture = 0;
		Integer calories = 0;
		for (Ingredient i : receipe.keySet()) {
			Integer amount = receipe.get(i);
			capacity += i.getCapacity() * amount;
			durability += i.getDurability() * amount;
			flavor += i.getFlavor() * amount;
			texture += i.getTexture() * amount;
			calories += i.getCalories() * amount;
		}
		capacity = Math.max(0, capacity);
		durability = Math.max(0, durability);
		flavor = Math.max(0, flavor);
		texture = Math.max(0, texture);

		return (calories == 500 ? capacity * durability * flavor * texture : 0);
	}
}