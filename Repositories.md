| Whom | Where | What |
|:------------:|:-----------------------------------------------------------------:|:-------:|
| Alfred456654 | [GitLab/Alfred456654](https://www.gitlab.com/Alfred456654/adventofcode) | Go & Python 3 |
| Charles | [GitLab/pycheric](https://gitlab.com/picheryc/advent_of_code_2019) | Python 3 |
| Benjamin | [GitLab/baudren](https://gitlab.com/baudren/adventofcode) | Elixir & Python 3 |
| Pierre | [GitLab/pelyot](https://gitlab.com/pelyot/adventofcode) | Rust |
| Sylvain | [Repl.it/syllabus](https://repl.it/@syllabus/AoC2019) | Clojure |
