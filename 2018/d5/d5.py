#!/usr/bin/python

import regex


RX = r'(\p{L})(?!\1)(?i:\1)'


def read_file(filename):
    with open(filename, 'r') as of:
        return of.readlines()[0].strip()


def react_full(formula):
    transform = react(formula)
    while transform != formula:
        formula = transform
        transform = regex.sub(RX, '', formula)
    return len(transform)


def calc(filename):
    return react_full(read_file(filename))


def calc2(filename):
    formula = read_file(filename)
    results = {}
    for l in range(26):
        results.update({chr(l+65): react_full(formula.replace(chr(65+l), '').replace(chr(97+l), ''))})
    return min(results.values())


if __name__ == '__main__':
    assert calc('test') == len('dabCBAcaDA')
    assert calc2('test') == len('daDA')
    print(calc('input'))
    print(calc2('input'))
