#!/usr/bin/python

import numpy as np


def read_file(filename):
    with open(filename, 'r') as of:
        lines = list(map(lambda l: np.sum([(1, 1), tuple(map(int, l.strip().split(', ')))], axis=0), of.readlines()))
        coords = {}
        for l in range(len(lines)):
            coords.update({1+l: lines[l]})
        return coords


def make_grid(coords):
    dim = max(max(coords.values(), key = lambda t: t[0])[0], max(coords.values(), key=lambda t: t[1])[1]) + 2
    dims = (dim, dim)
    grid = np.zeros(dims, dtype=int)
    for i in range(dims[0]):
        for j in range(dims[1]):
            dists = {c: np.abs(i-v[0]) + np.abs(j-v[1]) for c, v in coords.items()}
            closest_dist = min(dists.values())
            count = {c: d for c, d in dists.items() if d == closest_dist}
            closest_points = [k for k, t in count.items() if t == closest_dist]
            if len(closest_points) == 1:
                grid[i, j] = closest_points[0]
    return grid


def calc(filename):
    coords = read_file(filename)
    grid = make_grid(coords)
    small = grid[1:-1, 1:-1]
    a = dict(zip(*np.unique(grid, return_counts=True)))
    b = dict(zip(*np.unique(small, return_counts=True)))
    not_grown = {k: v for k, v in a.items() if b[k] == v}
    return max(not_grown.values())


def calc2(filename, max_allowed):
    coords = read_file(filename)
    dim_sup = max(max(coords.values(), key = lambda t: t[0])[0], max(coords.values(), key=lambda t: t[1])[1]) + 2
    dim_inf = 0
    grid = {}
    grid = [sum([np.abs(i-v[0]) + np.abs(j-v[1]) for c, v in coords.items()]) for i in range(dim_inf, dim_sup) for j in range(dim_inf, dim_sup)]
    return len([v for v in grid if v < max_allowed])


if __name__ == '__main__':
    assert calc('test') == 17
    print(calc('input'))
    assert calc2('test', 32) == 16
    print(calc2('input', 10000))
