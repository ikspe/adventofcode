import sys
def transform(in_str):
    txt = in_str.strip().replace('(', '#(').replace(')', ')#')
    cur_dict = []
    for t in txt.split('#'):
        if len(t) > 0:
            cur_dict.append({'rep': 1, 'txt': t})
    return cur_dict
def analyze(cur_dict):
    new_dict = []
    mode = 0
    expected_len = 0
    new_part = ''
    nb_parts = len(cur_dict)
    cur_idx = -1
    new_dict_idx = -1
    nb_rep = 1
    while cur_idx < nb_parts - 1:
        cur_idx += 1
        p = cur_dict[cur_idx]['txt']
        if len(p) > 0:
            if mode == 0:
                new_dict_idx += 1
                if p[0] == '(':
                    mode = 1
                    expected_len = int(p.split('(')[1].split('x')[0])
                    nb_rep = int(p.split('x')[1].split(')')[0])
                    new_part = ''
                    new_dict.append({'rep': nb_rep, 'txt': ''})
                else:
                    nb_rep = 1
                    new_dict.append({'rep': nb_rep, 'txt': p})
            elif mode == 1:
                if len(p) < expected_len - len(new_part):
                    new_part += p
                elif len(p) == expected_len - len(new_part):
                    new_part += p
                    mode = 0
                    new_dict[new_dict_idx]['txt'] = new_part
                    nb_rep = 1
                    new_part = ''
                else:
                    remaining = expected_len - len(new_part)
                    new_part += p[0:remaining]
                    mode = 0
                    new_dict[new_dict_idx]['txt'] = new_part
                    nb_rep = 1
                    cur_dict[cur_idx]['txt'] = p[remaining:]
                    new_part = ''
                    cur_idx -= 1
    new_dict.append({'rep': nb_rep, 'txt': new_part})
    for item in new_dict:
        if len(item['txt']) == 0:
            new_dict.remove(item)
    return new_dict
def recurse(cur_dict):
    cur_dict = analyze(cur_dict)
    for idx in range(len(cur_dict)):
        item = cur_dict[idx]
        cur_finished = item['txt'].count('(') == 0
        if not cur_finished:
            cur_dict[idx]['txt'] = recurse(transform(item['txt']))
    return cur_dict
def recurse_calc(cur_dict):
    answer = 0
    for item in cur_dict:
        if type(item['txt']) is str:
            answer += item['rep'] * len(item['txt'])
        elif type(item['txt']) is list:
            answer += item['rep'] * recurse_calc(item['txt'])
    return answer
if __name__ == '__main__':
    for j in sys.argv[1:]:
        with open(j) as f:
            result = recurse(transform(f.readline()))
            print(recurse_calc(result))
