import numpy as np
SIZE_X = 50
SIZE_Y = 6
def print_table(table):
    for i in range(table.shape[0]):
        for j in range(table.shape[1]):
            if table[i, j] == 1:
                print('♥', end="")
            else:
                print(' ', end="")
        print('')
if __name__ == '__main__':
    table = np.zeros((SIZE_Y, SIZE_X))
    with open('input', 'r') as f:
        for instr in f:
            args = instr.split(' ')
            if args[0] == 'rect':
                dim_x = int(args[1].split('x')[0])
                dim_y = int(args[1].split('x')[1])
                for i in range(dim_y):
                    for j in range(dim_x):
                        table[i][j] = 1
            elif args[0] == 'rotate':
                pixels = int(args[4])
                line_index = int(args[2].split('=')[1])
                if args[1] == 'row':
                    new_line = [ 0 ] * SIZE_X
                    for i in range(SIZE_X):
                        new_line[i] = table[line_index, (i - pixels) % SIZE_X]
                    table[line_index] = new_line
                elif args[1] == 'column':
                    new_line = [ 0 ] * SIZE_Y
                    for j in range(SIZE_Y):
                        new_line[j] = table[(j - pixels) % SIZE_Y, line_index]
                    table[:, line_index] = new_line
    print_table(table)
    print((1 == np.array(table)).sum())
