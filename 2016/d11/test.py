def valid_moves(building):
    moves = []
    elev_pos = building[10]
    items = building[:10]
    if elev_pos < 0 or elev_pos > 3:
        return moves
    m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos ]
    gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos ]

    if elev_pos > 0:
        below_m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos - 1 ]
        below_gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos - 1 ]
        for m in m_chips:
            if m in below_gens or len(below_gens) == 0:
                moves.append((-1, [2*m+1]))
            if m in gens:
                move_gen_ok = True
                for mm in below_m_chips:
                    if mm != m and mm not in below_gens:
                        move_gen_ok = False
                        break
                if move_gen_ok:
                    moves.append((-1, [2*m, 2*m+1]))
            for mm in m_chips:
                if m > mm:
                    if (m in below_gens and mm in below_gens) or len(below_gens) == 0:
                        moves.append((-1, [2*m+1, 2*mm+1]))
        for g in gens:
            move_gen_ok = (g not in m_chips or len(gens) < 2)
            if move_gen_ok:
                for m in below_m_chips:
                    if m != g and m not in below_gens:
                        move_gen_ok = False
                        break
            if move_gen_ok:
                moves.append((-1, [2*g]))
                for gg in gens:
                    if g > gg:
                        move_gg_ok = gg not in m_chips
                        if move_gg_ok:
                            for mm in below_m_chips:
                                if mm != gg and mm not in below_gens:
                                    move_gg_ok = False
                                    break
                        if move_gg_ok:
                            moves.append((-1, [2*g, 2*gg]))

    if elev_pos < 3:
        above_m_chips = [ i for i, x in enumerate(items[1::2]) if x == elev_pos - 1 ]
        above_gens = [ i for i, x in enumerate(items[::2]) if x == elev_pos - 1 ]
        for m in m_chips:
            if m in above_gens or len(above_gens) == 0:
                moves.append((1, [2*m+1]))
            if m in gens:
                move_gen_ok = True
                for mm in above_m_chips:
                    if mm != m and mm not in above_gens:
                        move_gen_ok = False
                        break
                if move_gen_ok:
                    moves.append((1, [2*m, 2*m+1]))
            for mm in m_chips:
                if m > mm:
                    if (m in above_gens and mm in above_gens) or len(above_gens) == 0:
                        moves.append((1, [2*m+1, 2*mm+1]))
        for g in gens:
            move_gen_ok = (g not in m_chips or len(gens) < 2)
            if move_gen_ok:
                for m in above_m_chips:
                    if m != g and m not in above_gens:
                        move_gen_ok = False
                        break
            if move_gen_ok:
                moves.append((1, [2*g]))
                for gg in gens:
                    if g > gg:
                        move_gg_ok = gg not in m_chips
                        if move_gg_ok:
                            for mm in above_m_chips:
                                if mm != gg and mm not in above_gens:
                                    move_gg_ok = False
                                    break
                        if move_gg_ok:
                            moves.append((1, [2*g, 2*gg]))
    return moves

def make_move(building, move):
    for obj in move[1]:
        building[obj] += move[0]
    building[10] += move[0]
    return building

def show(building):
    print('')
    print('  1122334455E')
    print('  GMGMGMGMGM')
    for y in range(4):
        line = '%d ' % (3 - y)
        for x in building:
            if x == 3 - y:
                line += 'X'
            else:
                line += ' '
        print(line)
    print('')

def get_hash(building):
    return ''.join(list(map(str, building)))

if __name__ == '__main__':
    init_building = [0, 0, 1, 2, 1, 2, 1, 2, 1, 2, 0]
    init_hash = get_hash(init_building)
    target = '33333333333'
    building = [ x for x in init_building ]
    queue = [ building ]
    visited = [ init_hash ]
    tree = { }
    while queue:
        if len(queue) % 100 == 0:
            print(len(queue))
        building = queue.pop(0)
        #show(building)
        h = get_hash(building)
        if h == target:
            fallback = h
            cpt = 0
            while fallback != init_hash:
                fallback = tree[fallback]
                cpt += 1
            print(cpt)
            break
        moves = valid_moves(building)
        #print('%d moves possible: %s' % (len(moves), moves))
        for m in moves:
            new_building = make_move([x for x in building], m)
            new_hash = get_hash(new_building)
            if new_hash not in visited:
                visited.append(new_hash)
                queue.append(new_building)
                tree.update({new_hash: h})
