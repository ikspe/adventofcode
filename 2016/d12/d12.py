if __name__ == '__main__':
    l_i = [ ]
    nb = 0
    #regs = { 'a': 0, 'b': 0, 'c': 0, 'd': 0 }
    regs = { 'a': 0, 'b': 0, 'c': 1, 'd': 0 }
    for i in list(map(str.strip, open('input', 'r').readlines())):
        l_i.append(i.split(' '))
    cur = 0
    while cur < len(l_i):
        nb += 1
        cur_i = l_i[cur]
        if cur_i[0] == 'cpy':
            src = cur_i[1]
            if src in regs:
                regs[cur_i[2]] = regs[src]
            else:
                regs[cur_i[2]] = int(src)
            cur += 1
        elif cur_i[0] == 'dec':
            regs[cur_i[1]] -= 1
            cur += 1
        elif cur_i[0] == 'inc':
            regs[cur_i[1]] += 1
            cur += 1
        elif cur_i[0] == 'jnz':
            src = cur_i[1]
            if src in regs:
                val = regs[src]
            else:
                val = int(src)
            if val != 0:
                cur += int(cur_i[2])
            else:
                cur += 1
    print(regs)
    print(nb)
