#!/usr/bin/python

test_k = [0, 3, 0, 1, -3]
test_v = 5
test_v2 = 10

def calc(in_strs):
    c, i, l = 0, 0, len(in_strs)
    while i in range(l):
        n_i = i + in_strs[i]
        in_strs[i] += 1
        c += 1
        i = n_i
    return c

def calc2(in_strs):
    c, i, l = 0, 0, len(in_strs)
    while i in range(l):
        n_i = i + in_strs[i]
        in_strs[i] += [-1, 1][in_strs[i] < 3]
        c += 1
        i = n_i
    return c

if __name__ == '__main__':
    # print('%d | %d' % (test_v, calc(test_k.copy())))
    assert calc(test_k.copy()) == test_v
    # print('%d | %d' % (test_v2, calc2(test_k.copy())))
    assert calc2(test_k.copy()) == test_v2
    with open('input', 'r') as f:
        in_strs = list(map(int, f.read().strip().split('\n')))
        a1 = calc(in_strs.copy())
        print('answer 1: %d' % a1)
        a2 = calc2(in_strs.copy())
        print('answer 2: %d' % a2)
