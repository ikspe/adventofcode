#!/usr/bin/python

import numpy as np
import operator

#test_k = np.array([0, 2, 7, 0])
#test_v = 5
#test_v2 = 4

def calc(mem):
    already_visited = {}
    #already_visited = []
    it = 0
    current = str(mem)
    l = len(mem)
    while current not in already_visited.values():
    #while current not in already_visited:
        already_visited[it] = current
        #already_visited.append(current)
        m_i, m_v = max(enumerate(mem), key = operator.itemgetter(1))
        mem[m_i] = 0
        mem += int(m_v / l)
        r = m_v % l
        e = m_i + r
        if e < l:
            mem[range(m_i + 1, e + 1)] += 1
        else:
            mem[range(m_i - l + 1, (e + 1) % l)] += 1
        current = str(mem)
        it += 1
    loop_size = it - [k for k in already_visited.keys() if already_visited[k] == current][0]
    #loop_size = it - already_visited.index(current)
    return it, loop_size

if __name__ == '__main__':
    #print('%s | %d,%d | %s' % (test_k, test_v, test_v2, calc(test_k.copy())))
    #assert (test_v, test_v2) == calc(test_k.copy())
    with open('input', 'r') as f:
        mem = np.array(list(map(int, f.read().strip().split('\t'))))
        a1, a2 = calc(mem.copy())
        print('answer 1: %d' % a1)
        print('answer 2: %d' % a2)
