#!/usr/bin/python

test = {'txt': '5\t1\t9\t5\n7\t5\t3\n2\t4\t6\t8', 'res': 18}
test2 = {'txt': '5\t9\t2\t8\n9\t4\t7\t3\n3\t8\t6\t5', 'res': 9}

def remap(in_str):
    return [ list(map(int, y.split('\t'))) for y in in_str.split('\n') ]

def calc(d):
    return sum([max(x) - min(x) for x in d])

def calc2(d):
    return sum([[x/y for x in z for y in z if x % y == 0 and y != x][0] for z in d])

if __name__ == '__main__':
    assert test['res'] == calc(remap(test['txt']))
    assert test2['res'] == calc2(remap(test2['txt']))
    with open('input', 'r') as f:
        in_str = f.read().strip()
        d = remap(in_str)
        print("answer 1: %d" % calc(d))
        print("answer 2: %d" % calc2(d))
