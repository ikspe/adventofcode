#!/usr/bin/python

from collections import deque

OPS = {'add': '+', 'mul': '*', 'mod': '%'}

def calc2(instrs):
    count = 0
    pt = {0: 0, 1: 0}
    regs = {0: {'p' :0}, 1: {'p': 1}}
    cur = 0
    states = {0: 0, 1: 0}
    queues = {0: deque(), 1: deque()}
    while sum(states.values()) != 4:
        i = instrs[pt[cur]]
        y = 0
        if len(i) > 2:
            y = regs[cur][i[2]] if i[2] in regs[cur] else int(i[2])
        if i[1].replace('-', '').isdigit():
            regs[cur][i[1]] = int(i[1])
        elif i[1] not in regs[cur]:
            regs[cur][i[1]] = 0
        if i[0] == 'set':
            regs[cur][i[1]] = y
        elif i[0] in ['add', 'mul', 'mod']:
            regs[cur][i[1]] = eval('%s %s %s' % (regs[cur][i[1]], OPS[i[0]], y))
        elif i[0] == 'snd':
            queues[1-cur].append(regs[cur][i[1]])
            if cur == 1:
                count += 1
        elif i[0] == 'jgz':
            if regs[cur][i[1]] > 0:
                pt[cur] += y - 1
        elif i[0] == 'rcv':
            if len(queues[cur]) > 0:
                regs[cur][i[1]] = queues[cur].popleft()
                states[cur] = 0
            else:
                states[cur] = 1
                if states[1-cur] == 0 or (states[1-cur] == 1 and len(queues[1-cur]) > 0):
                    cur = 1-cur
                    continue
                else:
                    states[1-cur] = 2
                    states[cur] = 2
                    return count
        pt[cur] += 1
        if pt[cur] == len(instrs) - 1:
            states[cur] = 2
    return count

def calc(instrs):
    last_sound = 0
    pointer = 0
    regs = {}
    while pointer < len(instrs) - 1:
        i = instrs[pointer]
        y = 0
        if len(i) > 2:
            y = regs[i[2]] if i[2] in regs else int(i[2])
        if i[1] not in regs:
            regs[i[1]] = 0
        if i[0] == 'set':
            regs[i[1]] = y
        if i[0] in ['add', 'mul', 'mod']:
            regs[i[1]] = eval('%s %s %s' % (regs[i[1]], OPS[i[0]], y))
        elif i[0] == 'snd':
            last_sound = regs[i[1]]
        elif i[0] == 'rcv':
            if regs[i[1]] != 0:
                return last_sound
        elif i[0] == 'jgz':
            if regs[i[1]] > 0:
                pointer += y - 1
        pointer += 1

if __name__ == '__main__':
    with open('input', 'r') as f:
        instrs = [x.split(' ') for x in f.read().strip().split('\n')]
        print('Answer 1: %d' % calc(instrs))
        print('Answer 2: %d' % calc2(instrs))
